// Display Manager
// Brandon Packard
// 11/14/2015
//
// Simple triple class, which holds 2 ints and a char.

public class Triple
{
	int x;
	int y; 
	char symbol;
	
	Triple(int xIn, int yIn, char symbolIn)
	{
		x = xIn;
		y = yIn;
		symbol = symbolIn;
	}
}