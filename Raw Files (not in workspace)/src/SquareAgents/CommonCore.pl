numMines(List, Result) :- numMines(List, 0, Result).
numMines([],ReturnNum,ReturnNum) :- !.
numMines([H|T],CurrentNum,ReturnNum) :- 
	mineLine(H,NumInLine), NewNum is CurrentNum + NumInLine, numMines(T,NewNum,ReturnNum).


mineLine([*,*,*], X) :- !,X = 3.
mineLine([_,*,*], X) :- !,X = 2.
mineLine([*,_,*], X) :- !,X = 2.
mineLine([*,*,_], X) :- !,X = 2.
mineLine([*,_,_], X) :- !,X = 1.
mineLine([_,*,_], X) :- !,X = 1.
mineLine([_,_,*], X) :- !,X = 1.
mineLine([_,_,_], X) :- !,X = 0.

mineLine([*,*],X) :- !, X = 2.
mineLine([*,_],X) :- !, X = 1.
mineLine([_,*],X) :- !, X = 1.
mineLine([_,_],X) :- !, X = 0.



%Count and report location of blanks
numBlanks(List, Result) :- numBlanks(List, 0, Result).
numBlanks([],ReturnNum,ReturnNum) :- !.
numBlanks([H|T],CurrentNum,ReturnNum) :- 
	blankLine(H,NumInLine), NewNum is CurrentNum + NumInLine, numBlanks(T,NewNum,ReturnNum).


blankLine([-,-,-], X) :- !,X = 3.
blankLine([_,-,-], X) :- !,X = 2.
blankLine([-,_,-], X) :- !,X = 2.
blankLine([-,-,_], X) :- !,X = 2.
blankLine([-,_,_], X) :- !,X = 1.
blankLine([_,-,_], X) :- !,X = 1.
blankLine([_,_,-], X) :- !,X = 1.
blankLine([_,_,_], X) :- !,X = 0.

blankLine([-,-],X) :- !, X = 2.
blankLine([-,_],X) :- !, X = 1.
blankLine([_,-],X) :- !, X = 1.
blankLine([_,_],X) :- !, X = 0.

	
getClickList(List, Result) :- getClickList(List, [[1,2,3],[4,5,6],[7,8,9]],[], Result).
getClickList([],[],ReturnList,ReturnList) :- !.
getClickList(_,[],ReturnList,ReturnList) :- !.
getClickList([],_,ReturnList,ReturnList) :- !.
getClickList([H|T],[H2|T2],CurrentList,ReturnList) :- 
				clickLine(H,H2,[],List), append(CurrentList, List, NewList), getClickList(T,T2,NewList,ReturnList).

clickLine(List,X) :- clickLine(List, [1,2,3], [], X).
clickLine([],[],ReturnList,ReturnList) :- !.
clickLine([H|T],[H2|T2],CurrList,ReturnList) :- H = '-',!,append(CurrList,[H2],NewCurr),  
         	clickLine(T,T2,NewCurr,ReturnList).
clickLine([_H|T],[_H2|T2],CurrList,ReturnList) :-!,clickLine(T,T2,CurrList,ReturnList).
			
minorBC([[A,B,C],[_D,E,_F],[_G,H,_I]],X):- H \= (-), H \= * , H \= '@',
	H < E, 
	numMines([[A,B,C],[-1,-1,-1],[-1,-1,-1]], N),% numBlanks([[-1,-1,-1],[D,E,F],[G,H,I]],NumBlank), 
	%plus(N, NumBlank, Total), Total = H,
	NumLeft is E - H - N,
	numBlanks([[A,B,C],[-1,-1,-1],[-1,-1,-1]], NB), NB = NumLeft, !, % plus(NB,N,NumLeft)
	getClickList([[A,B,C]],List), X = List.
minorBC(_,[]).
	
minorBR([[A,B,C],[D,E,_F],[G,_H,I]],X):- I \= (-), I \= * , I \= '@',
	I < E, 
	numMines([[A,B,C],[D,-1,-1],[G,-1,-1]], N),% numBlanks([[-1,-1,-1],[-1,E,F],[-1,H,I]],NumBlank),
	%plus(N, NumBlank, Total), Total = I,
	NumLeft is E - I - N,
	numBlanks([[A,B,C],[D,-1,-1],[G,-1,-1]], NB), NB = NumLeft, !,
	getClickList([[A,B,C],[D,-1,-1],[G,-1,-1]],List), X = List.
minorBR(_,[]).
	
minorCR([[A,_B,_C],[D,E,F],[G,_H,_I]],X):- F \= (-), F \= * , F \= '@',
	F < E, 
	numMines([[A,-1,-1],[D,-1,-1],[G,-1,-1]], N), %numBlanks([[-1,B,C],[-1,E,F],[-1,H,I]],NumBlank), 
	%plus(N, NumBlank, Total), Total = F,
	NumLeft is E - F - N,
	numBlanks([[A,-1,-1],[D,-1,-1],[G,-1,-1]], NB), NB = NumLeft, !,
	getClickList([[A,-1,-1],[D,-1,-1],[G,-1,-1]],List), X = List.
minorCR(_,[]).	
	
minorTR([[A,_B,C],[D,E,_F],[G,H,I]],X):- C \= (-), C \= * , C \= '@',
	C < E, 
	numMines([[A,-1,-1],[D,-1,-1],[G,H,I]], N),% numBlanks([[-1,B,C],[-1,E,F],[-1,-1,-1]],NumBlank), 
	%plus(N, NumBlank, Total), Total = C,
	NumLeft is E - C - N,
	numBlanks([[A,-1,-1],[D,-1,-1],[G,H,I]], NB), NB = NumLeft, !,
	getClickList([[A,-1,-1],[D,-1,-1],[G,H,I]],List), X = List.
minorTR(_,[]).
	
minorTC([[_A,B,_C],[_D,E,_F],[G,H,I]],X):- B \= (-), B \= * , B \= '@',
	B < E, 
	numMines([[-1,-1,-1],[-1,-1,-1],[G,H,I]], N),% numBlanks([[A,B,C],[D,E,F],[-1,-1,-1]],NumBlank), 
	%plus(N, NumBlank, Total), Total = B,
	NumLeft is E - B - N,
	numBlanks([[-1,-1,-1],[-1,-1,-1],[G,H,I]], NB), NB = NumLeft, !,
	getClickList([[-1,-1,-1],[-1,-1,-1],[G,H,I]],List), X = List.
minorTC(_,[]).
	
minorTL([[A,_B,C],[_D,E,F],[G,H,I]],X):- A \= (-), A \= * , A \= '@',
	A < E, 
	numMines([[-1,-1,C],[-1,-1,F],[G,H,I]], N),% numBlanks([[A,B,-1],[D,E,-1],[-1,-1,-1]],NumBlank), 
	%plus(N, NumBlank, Total), Total = A,
	NumLeft is E - A - N,
	numBlanks([[-1,-1,C],[-1,-1,F],[G,H,I]], NB), NB = NumLeft, !,
	getClickList([[-1,-1,C],[-1,-1,F],[G,H,I]],List), X = List.
minorTL(_,[]).

minorCL([[_A,_B,C],[D,E,F],[_G,_H,I]],X):- D \= (-), D \= * , D \= '@',
	D < E, 
	numMines([[-1,-1,C],[-1,-1,F],[-1,-1,I]], N), %numBlanks([[A,B,-1],[D,E,-1],[G,H,-1]],NumBlank), 
	%plus(N, NumBlank, Total), Total = D,
	NumLeft is E - D - N,
	numBlanks([[-1,-1,C],[-1,-1,F],[-1,-1,I]], NB), NB = NumLeft, !,
	getClickList([[-1,-1,C],[-1,-1,F],[-1,-1,I]],List), X = List.
minorCL(_,[]).

minorBL([[A,B,C],[_D,E,F],[G,_H,I]],X):- G \= (-), G \= * , G \= '@',
	G < E, 
	numMines([[A,B,C],[-1,-1,F],[-1,-1,I]], N), %numBlanks([[-1,-1,-1],[D,E,-1],[G,H,-1]],NumBlank), 
	%plus(N, NumBlank, Total), Total = G,
	NumLeft is E - G - N,
	numBlanks([[A,B,C],[-1,-1,F],[-1,-1,I]], NB), NB = NumLeft, !,
	getClickList([[A,B,C],[-1,-1,F],[-1,-1,I]],List), X = List.
minorBL(_,[]).




allMinor([[A,B,C],[D,E,F],[G,H,I]],ClickList) :- 
	minorTL([[A,B,C],[D,E,F],[G,H,I]],Click1), minorTC([[A,B,C],[D,E,F],[G,H,I]],Click2), minorTR([[A,B,C],[D,E,F],[G,H,I]],Click3), minorCL([[A,B,C],[D,E,F],[G,H,I]],Click4),
	minorCR([[A,B,C],[D,E,F],[G,H,I]],Click5), minorBL([[A,B,C],[D,E,F],[G,H,I]],Click6), minorBC([[A,B,C],[D,E,F],[G,H,I]],Click7), minorBR([[A,B,C],[D,E,F],[G,H,I]],Click8),
	union(Click1, Click2, Temp), union(Temp, Click3, Temp2), union(Temp2, Click4, Temp3), union(Temp3, Click5, Temp4), union(Temp4, Click6, Temp5), union(Temp5, Click7, Temp6), union(Temp6, Click8, ClickList),!.
	%print(Click1),nl,print(Click2),nl,print(Click3),nl,print(Click4),nl,print(Click5),nl,print(Click6),nl,print(Click7),nl,print(Click8).