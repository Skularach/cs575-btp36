% Code for each square agent
% Brandon Packard
% Written for CS575, Fa 2015
% 11/2/2015
%
% Purpose:
% Given a 3x3 section of a minesweeper grid, return one of the following things:
%       nothing -- Indicates that this agent was unable to derive any new information
%       click:[List] -- Indicates that all squares in List are safe to click
%       mine:[List] -- Indicates that all squares in List are known to be mines
%
%       Each grid square is refered to by a number when being passed back.  The numbering scheme is as follows:
%			-------------
%           | 1 | 2 | 3 |  
%			-------------
%           | 4 | 5 | 6 |
%			-------------
%           | 7 | 8 | 9 |
%			-------------
%
%	For each square, all of the following cases that are possible are checked:
%		(For example, if position 5 is a 2, only the 1 less and 2 less mines are checked, since the others are impossible)	
%
%                   Case															Solution
%		The number of mines we have is the same as the number in position 5	     	Mark all blanks as able to be clicked
%		We have 1 less mine than in position 5, and 1 blank							Mark all blanks as mines
%       We have 2 less mines than in position 5, and 2 blanks						Mark all blanks as mines
%		etc, up to 8 less mines																	''
%				
%		We know that some squares must be mines due to a combination of 			Mark those squares as mines
%				position 5 and another number in the 3x3 grid
%
%		None of the 9 squares are blanks											Return that no information can ever be gained
%
%		None of the above cases apply												Return that no information can be gained at this time
%
%
:- multifile deduce/2.

deduce([[A,B,C],[D,2,F],[G,H,I]],X):-  
	numMines([[A,B,C],[D,2,F],[G,H,I]], N), N = 2,  
	getClickList([[A,B,C],[D,2,F],[G,H,I]],L), L \= [],!,X = (click : L).
	%X = 'click:all'.
	
deduce([[A,B,C],[D,2,F],[G,H,I]],X):-  
	numMines([[A,B,C],[D,2,F],[G,H,I]], N), N = 1,
	numBlanks([[A,B,C],[D,2,F],[G,H,I]], NB), NB = 1, !, 
	getClickList([[A,B,C],[D,2,F],[G,H,I]],L), X = (mine : L).

	
deduce([[A,B,C],[D,2,F],[G,H,I]],X):-  
	numMines([[A,B,C],[D,2,F],[G,H,I]], N), N = 0,
	numBlanks([[A,B,C],[D,2,F],[G,H,I]], NB), NB = 2, !, 
	getClickList([[A,B,C],[D,2,F],[G,H,I]],L), X = (mine : L).
	
deduce([[A,B,C],[D,2,F],[G,H,I]],X):- 
	allMinor([[A,B,C],[D,2,F],[G,H,I]],Mines),Mines \= [],!,X = (mine : Mines).
	
deduce([[A,B,C],[D,2,F],[G,H,I]],X):-  
	numBlanks([[A,B,C],[D,2,F],[G,H,I]], NB), NB = 0, 
	X = 'done',!.
	
deduce([[_,_,_],[_,2,_],[_,_,_]],X):- !, X = 'nothing'.