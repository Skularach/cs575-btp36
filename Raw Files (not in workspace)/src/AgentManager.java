// Agent Manager
// Brandon Packard
// 11/14/2015
//
// This component keeps a list of active agents and, when prompted to by the Board Manager, 
//   queries them to obtain newly derived information about the board.

import java.util.ArrayList;
import java.util.Hashtable;

import jpl.Term;

public class AgentManager
{
	public  ArrayList<SquareAgent> agents = new ArrayList<SquareAgent>();
	static int AGENTSIZE = BoardManager.AGENTSIZE;
	static int BOARDSIZE = BoardManager.BOARDSIZE;
	
	//Each agent returns data relative to its own 3x3 grid (since it has no knowledge of the bigger board)
	//   This method parses that localized information and turns it into global information for use by the board manager
	//   Return values are in the form of (xPos in grid, yPos in grid, data)
	private static ArrayList<Triple> parseAgentResponse(int agentX, int agentY, Hashtable solution)
	{
		ArrayList<String> returnList = new ArrayList<String>();
		ArrayList<Triple> tripleList = new ArrayList<Triple>();
		
		Object result_binding = solution.get("Result");
		
		//after this loop, startTerm.name indicates if the agent is providing the location of mines,
		// what squares are clickable, or if it can't derive anything, and returnList holds
		// the positions that this applies to, relative to that agent's location.
		Term startTerm = ((jpl.Term)result_binding);;
		while(startTerm.args().length > 0)
		{
			returnList.add(startTerm.args()[0].toString());
			startTerm = startTerm.args()[1];
		}	
		
		//Agent could not derive any information
		if(returnList.size() == 0)
		{
			if(startTerm.name().equals("nothing"))
				return tripleList; 
			
			if(startTerm.name().equals("done"))
				return null;
		}
		
		else 
			//For each square in the return list, make a triple that contains the absolute
			// locations of that square on the board, and a symbol denoting what information is known about that square. 
			for(int i = 1; i < returnList.size(); i++)
			{
				int selectedSquare = Integer.parseInt(returnList.get(i));
				
				//If location 1, that is above and to the left of the current agent,
				// so x-1 and Y-1.  Do this for all 8 cases.
				int xOffset = 0;
				int yOffset = 0;
				if(selectedSquare == 1 || selectedSquare == 2 || selectedSquare == 3)
					xOffset = -1;
				if(selectedSquare == 7 || selectedSquare == 8 || selectedSquare == 9)
					xOffset = 1;
				if(selectedSquare == 1 || selectedSquare == 4 || selectedSquare == 7)
					yOffset = -1;
				if(selectedSquare == 3 || selectedSquare == 6 || selectedSquare == 9)
					yOffset = 1;
				
				Triple newTriple;
				
				//if the specified location is off the board, ignore it (happens with agents that are on the edge of the board)
				//  otherwise, create a triple with that information.  Note that * means the location holds a mine, and 'c' means
				//  the location can be clicked.
				if(agentX+xOffset >= 0 && agentX + xOffset < BOARDSIZE && agentY+yOffset >= 0 && agentY + yOffset < BOARDSIZE)
				{
					if(returnList.get(0).equals("mine"))
						newTriple = new Triple(agentX + xOffset, agentY + yOffset, '*');
					else 
						newTriple = new Triple(agentX + xOffset, agentY + yOffset, 'c');
					
					tripleList.add(newTriple);
				}
			}
		
		return tripleList;
	}

	//Query each agent one by one for information, then return all that information as one big list.
	// Also removes any agents which can never give any more information
	public ArrayList<Triple> queryAgents(char[][] currentGrid) {
		ArrayList<Triple> allResponses = new ArrayList<Triple>();
		ArrayList<SquareAgent> finishedAgents = new ArrayList<SquareAgent>();
		for(int i = 0; i < agents.size(); i++)
		{
			int x = agents.get(i).x;
			int y = agents.get(i).y;
			
			//extract the agents 3x3 grid from the overall grid
			//pad with @s where the 3x3 would be off the grid
			char[][] localGrid = new char[AGENTSIZE][AGENTSIZE];
			for(int j = -1; j <= 1; j++)
				for(int k = -1; k <= 1; k++)
				{
					//within boundaries
					if(x+j >= 0 && x+j < BOARDSIZE && y+k >= 0 && y+k < BOARDSIZE)
						localGrid[j+1][k+1] = currentGrid[x+j][y+k];
					else
						localGrid[j+1][k+1] = '@'; //The Prolog portion knows to ignore this symbol
				}
			
			//Get each response and add them to a list
			Hashtable agentResponse = agents.get(i).generateInfo(localGrid);
			ArrayList<Triple> responses = parseAgentResponse(x,y,agentResponse);
			
			if(responses == null)
				finishedAgents.add(agents.get(i));
			else
				allResponses.addAll(responses);
		}
			
		//Remove all agents that are done giving responses
		for(int i = 0; i < finishedAgents.size(); i++)
			agents.remove(finishedAgents.get(i));
		
		return allResponses;
	}
	
	//Initialize a new agent and add it to the list
	public void addNewAgent(int x, int y, char type)
	{
		SquareAgent newAgent = new SquareAgent(x,y,type);
		agents.add(newAgent);
	}
}