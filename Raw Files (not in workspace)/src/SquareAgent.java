// Square Agent
// Brandon Packard
// 11/14/2015
//
// This class is basically an intermediate between the java and prolog portions of the system.
// It holds the position and tile # of the agent, and has a method to prepare a query
//   and pass it through the JPL library, returning the result of the query.

import java.util.Hashtable;

import jpl.Compound;
import jpl.Query;
import jpl.Term;
import jpl.Util;
import jpl.Variable;

public class SquareAgent
{
	int x;
	int y;
	char type;
	
	SquareAgent(int xIn, int yIn, char typeIn)
	{
		x = xIn;
		y = yIn;
		type = typeIn;
	}

	//Given a minigrid of size 3x3 as a parameter, this method constructs a Prolog query to deduce information
	// from that minigrid, and returns the solution that is returned from the Prolog portion of the system.
	public Hashtable generateInfo(char[][] miniGrid) {
		Hashtable solution = Query.oneSolution(
			    new Compound("deduce", new Term[] {
			    		Util.textToTerm("[["+miniGrid[0][0]+","+miniGrid[0][1]+","+miniGrid[0][2]+"],["+miniGrid[1][0]+","+miniGrid[1][1]+","+miniGrid[1][2]+"],["+miniGrid[2][0]+","+miniGrid[2][1]+","+miniGrid[2][2]+"]]"),
			    		new Variable("Result")
			    })
			);

		return solution;
	}
}