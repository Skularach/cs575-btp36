// Display Manager
// Brandon Packard
// 11/14/2015
//
// Is responsible for the game board that the user sees, and updating it in such a way that
// it is easy for the user to follow what is going on

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class DisplayBoard extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	final int BOARDSIZE = BoardManager.BOARDSIZE;
	private JPanel[][] gridSquares = new JPanel[BOARDSIZE][BOARDSIZE];
	
	//set up the visual gameboard that the user sees
	public DisplayBoard()
	{
		setSize(400,400);
		GridLayout gameGrid = new GridLayout(BOARDSIZE,BOARDSIZE,5,5);
		this.setLayout(gameGrid);
		
		for(int i = 0; i < BOARDSIZE; i++)
			for(int j = 0; j < BOARDSIZE; j++)
			{
				gridSquares[i][j] = new JPanel();
				JLabel label = new JLabel();
				label.setFont(new Font("Veranda",1,20));
				label.setVerticalTextPosition(JLabel.CENTER);
				gridSquares[i][j].add(label);
				
				gridSquares[i][j].setBorder(new LineBorder(Color.BLACK));

				gridSquares[i][j].setSize(10,10);
				gridSquares[i][j].setBackground(Color.WHITE);
				this.add(gridSquares[i][j]);
			}
		
	}
	
	//Used at the start and when new tiles are revealed
	public void setPanel(int x, int y, String symbol)
	{
		((JLabel)gridSquares[x][y].getComponents()[0]).setText(symbol);
	}

	//sets the panel at the specified location to the specified color.
	//used to aid the user in seeing the changes made to the board
	public void setPanelColor(int x, int y, Color color) {
		gridSquares[x][y].setBackground(color);
		
	}
	
	//moving to a new iteration, so turn the past iteration blue
	public void setAllYellowToBlue()
	{
		for(int i = 0; i < gridSquares.length; i++)
			for(int j = 0; j < gridSquares.length; j++)
				if(gridSquares[i][j].getBackground() == Color.yellow)
					gridSquares[i][j].setBackground(Color.blue);
	}
	
	//Set up the GUI that displays the board and progress to the user.
	protected void initializeGui()
	{
		JFrame mainWindow = new JFrame("Autonomous Minesweeper");
		mainWindow.setSize(400,400);
		mainWindow.setVisible(true);
		mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainWindow.add(this);
	}
}