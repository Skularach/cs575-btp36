// Board Manager
// Brandon Packard
// 11/14/2015
//
// Is responsible for the maintaining the internal game boards (the working board and the
// full board that is hidden from the solvers).  Has the main running loop of the system.
// Calls methods in the Display Manager (DisplayBoard.java) and AgentManager as necessary.

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import jpl.Query;

public class BoardManager
{
	public final static int BOARDSIZE = 8;
	public final static int AGENTSIZE = 3;
	private static char[][] gameBoard = new char[BOARDSIZE][BOARDSIZE];
	private static DisplayBoard board;
	private static AgentManager manager;
	
	//Main loop of the system, runs user specified boards until they select to stop
	public static void main(String[] args)
	{
		loadPrologFiles();
		
		while(true)
		{
		
			//Ask the user what board to load, exitting if they wish to
			String[] choices = {"NonDeterministicExample1","NonDeterministicExample2","NumMinesExample1","SolvableExample1","SolvableExample2","SolvableExample3","Exit"};
			String fileChoice =(String) JOptionPane.showInputDialog(null, "Select an Example", "Select an example", JOptionPane.QUESTION_MESSAGE, null,choices, choices[0]);
			
			if(fileChoice == null || fileChoice.equals("Exit"))
				System.exit(0);
			
			//Initialize the Display and Agent Managers
			board = new DisplayBoard();
			board.initializeGui();
			manager = new AgentManager();
			
			//Store the starting grid and full grid
			char[][] fullGrid = readGridFromFile("DataFiles/"+fileChoice+".txt");
			char[][] startGrid = readGridFromFile("DataFiles/"+fileChoice+"Start.txt");
			gameBoard = startGrid;
			
			//Add an agent for each non-zero # in the starting grid
			for(int i = 0; i < startGrid.length; i++)
			{
				for(int j = 0; j < startGrid[0].length; j++)
				{
					board.setPanel(i, j, ""+startGrid[i][j]);
					if(startGrid[i][j] != '*' && startGrid[i][j] != '0' && startGrid[i][j] != '-')
						manager.addNewAgent(i,j,startGrid[i][j]);
				}
			}
			
			
			ArrayList<Triple> infoList = manager.queryAgents(startGrid);
			
			//while we are still getting information with each iteration...
			while(infoList.size() > 0)
			{
				//Pause for 3 seconds, otherwise the board fills so fast the user cannot see what is happening
				try{Thread.sleep(3000);}
				catch(Exception ex)
					{System.err.println("System was not able to properly pause");}
				
				//all previously deduced squares to blue to mark they were in a past iteration
				board.setAllYellowToBlue();
				
				for(int i = 0; i < infoList.size(); i++)
				{
					//Tile is a mine, set grid info and visuals accordingly
					if(infoList.get(i).symbol == '*')
					{
						startGrid[infoList.get(i).x][infoList.get(i).y] = infoList.get(i).symbol;
						board.setPanel(infoList.get(i).x, infoList.get(i).y, String.valueOf(infoList.get(i).symbol));
						board.setPanelColor(infoList.get(i).x, infoList.get(i).y, Color.yellow);
					}
					
					//Tile is clickable, click it and floodfill 0s / create new agents as necessary
					else if(infoList.get(i).symbol == 'c')
					{
						if(infoList.get(i).x >= 0 && infoList.get(i).x < BOARDSIZE && infoList.get(i).y >= 0 && infoList.get(i).y < BOARDSIZE)
						{
							char hiddenValue = fullGrid[infoList.get(i).x][infoList.get(i).y];
							
							if(Character.isDigit(hiddenValue) && hiddenValue != '0')
							{
								startGrid[infoList.get(i).x][infoList.get(i).y] = hiddenValue;
								board.setPanel(infoList.get(i).x, infoList.get(i).y, String.valueOf(hiddenValue));
								board.setPanelColor(infoList.get(i).x, infoList.get(i).y, Color.yellow);
	//							System.out.print(manager.agents);
								manager.addNewAgent(infoList.get(i).x, infoList.get(i).y, hiddenValue);
							}
							
							//floodfill 0s, like real minesweeper would
							else if(hiddenValue == '0')
							{
								startGrid = floodFillZeros(infoList.get(i).x,infoList.get(i).y, hiddenValue, startGrid, fullGrid);
							}
						}
							
					}
				}
				
				//Query for more information
				infoList = manager.queryAgents(startGrid);
			}	
				
			try{Thread.sleep(3000);}
			catch(Exception ex)
			{System.err.println("System was not able to properly pause");}
		
			//System.out.println("test");
		//turn all to blue to show nothing more can be derived
		board.setAllYellowToBlue();
		
		//Pause 5 seconds before allowing the user to select another board
		try{Thread.sleep(5000);}
		catch(Exception ex)
		{System.err.println("System was not able to properly pause");}

		}
	}
	
	
	//Called when a 0 is "clicked"
	//Fills in all 0s and all numbers adjacent to them, like in real minesweeper.
	private static char[][] floodFillZeros(int x, int y, char value, char[][] grid, char[][] fullGrid) {
		grid[x][y] = value;
		board.setPanel(x,y, String.valueOf(value));
		board.setPanelColor(x,y, Color.yellow);
		if(value == '0')
		{
			if(x-1 >= 0 && grid[x-1][y] == '-')
				grid = floodFillZeros(x-1, y, fullGrid[x-1][y], grid, fullGrid); 
			if(x+1 < BOARDSIZE && grid[x+1][y] == '-')
				grid = floodFillZeros(x+1, y, fullGrid[x+1][y], grid, fullGrid); 
			if(y-1 >= 0 && grid[x][y-1] == '-')
				grid = floodFillZeros(x, y-1, fullGrid[x][y-1], grid, fullGrid); 
			if(y+1 < BOARDSIZE && grid[x][y+1] == '-')
				grid = floodFillZeros(x, y+1, fullGrid[x][y+1], grid, fullGrid); 
		
			if(x-1 >= 0 && y-1 >= 0 && grid[x-1][y-1] == '-')
				grid = floodFillZeros(x-1, y-1, fullGrid[x-1][y-1], grid, fullGrid); 
			if(x+1 < BOARDSIZE && y+1 < BOARDSIZE && grid[x+1][y+1] == '-')
				grid = floodFillZeros(x+1, y+1, fullGrid[x+1][y+1], grid, fullGrid); 
			if(y-1 >= 0 && x+1 < BOARDSIZE && grid[x+1][y-1] == '-')
				grid = floodFillZeros(x+1, y-1, fullGrid[x+1][y-1], grid, fullGrid); 
			if(y+1 < BOARDSIZE && x-1 >= 0 && grid[x-1][y+1] == '-')
				grid = floodFillZeros(x-1, y+1, fullGrid[x-1][y+1], grid, fullGrid); 

		}
		return grid;
	}


	//Read in a minesweeper grid from a file
	private static char[][] readGridFromFile(String fileLocation)
	{
		char[][] returnGrid = new char[BOARDSIZE][BOARDSIZE];
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(fileLocation))))
		{
			int rowIndex = 0;
			String currentLine;
			while ((currentLine = reader.readLine()) != null)
			{
				String[] splitLine = currentLine.split(" ");
				
				for(int j = 0; j < splitLine.length; j++)
				{
				//	if(splitLine[j].charAt(0) != '-')
						returnGrid[rowIndex][j] = splitLine[j].charAt(0);
				}
				
				rowIndex++;
			}
		}
		catch(Exception e){System.err.println("Error reading in file");};
		
		return returnGrid;
	}
	
	//Load the common core and all 8 sets of agent code
	private static void loadPrologFiles()
	{
		String pwd = System.getProperty("user.dir");
		pwd = pwd.replace("\\", "/");
		
		for(int i = 1; i < 9; i++)
		{
			Query consult = new Query("consult('"+pwd+"/src/SquareAgents/SquareAgent"+i+".pl')");
			consult.allSolutions();
		}
		
		Query consult = new Query("consult('"+pwd+"/src/SquareAgents/CommonCore.pl')");
		consult.allSolutions();
				
	}
	
	
}